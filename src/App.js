import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import NavbarMenu from './navbar/components/navbar';
import ListVideos from './videos/containers/list-videos';

class App extends Component {

  state = {
    value: '',
    valueFinal: '',
    videos: []
  }

  handleSubmit = event => {
    event.preventDefault();
    this.setState({
      valueFinal: this.input.value,
      videos: []
    })
  }

  setInputRef = element => {
    this.input = element
  }


  render() {
    if (this.state.valueFinal != '') {
      this.props.data.videos.filter((item) => {
        return item.author.toLowerCase().includes(this.state.valueFinal.toLowerCase()) && this.state.videos.push(item) || item.title.toLowerCase().includes(this.state.valueFinal.toLowerCase()) && this.state.videos.push(item)
      })
    }
    console.log(this.state.videos)
    return (
      <div className="App">
        <NavbarMenu setInputRef={this.setInputRef} handleSubmit={this.handleSubmit} />
        {
          this.state.videos.length > 0 ?
            <ListVideos listVideos={this.state.videos} id="search" />
          : 
            this.state.valueFinal === '' ?
              <ListVideos listVideos={this.props.data.videos} id="list" />
            :
              <h2 className="text-center">No hay coincidencias</h2>
        }
      </div>
    );
  }
}

export default App;
