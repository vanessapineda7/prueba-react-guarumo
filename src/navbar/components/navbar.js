import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

function NavbarMenu(props) {
	return (
		<Navbar>
		  <Navbar.Header>
		    <Navbar.Brand>
		      <a href="/">React-Bootstrap</a>
		    </Navbar.Brand>
		    <Navbar.Toggle />
		  </Navbar.Header>
		  <Navbar.Collapse>
		    <Nav>
		      <NavItem eventKey={1} href="#">
		        Link
		      </NavItem>
		    </Nav>
		    <Navbar.Form pullLeft>
			   	<form className="form-inline" onSubmit={props.handleSubmit}>
			   		<div className="form-group">
					    <input 
					    	type="text" 
					    	className="form-control" 
					    	placeholder="Buscar" 
					    	name="search"
								ref={props.setInputRef}
					    />
					  </div>
					  <button type="button" className="btn btn-default" onClick={props.handleSubmit}>Buscar</button>
			   	</form>
			  </Navbar.Form>
		    <Nav pullRight>
		      <NavItem eventKey={1} href="#">
		        Link Right
		      </NavItem>
		      <NavItem eventKey={2} href="#">
		        Link Right
		      </NavItem>
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}


export default NavbarMenu;

