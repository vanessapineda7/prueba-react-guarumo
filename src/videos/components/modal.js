import React from 'react';
import { Modal, Button } from 'react-bootstrap';


function ModalVideo(props) {
	return (
		<Modal show={props.showModal} onHide={props.handleClose} >
      <Modal.Header closeButton>
        <Modal.Title>{props.video.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      	<iframe width="500" height="400" src={props.video.src}></iframe>
	      <p>{props.video.author}</p>
	      <p>{props.video.views}</p>
	      <p>{props.video.date}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.handleClose}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
	)
}


export default ModalVideo;