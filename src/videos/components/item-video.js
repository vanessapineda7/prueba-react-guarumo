import React, { Component } from 'react';
import { Thumbnail, Modal, Button } from 'react-bootstrap';
import './item-video.css';
import ModalVideo from '../components/modal';


class ItemVideo extends Component {
	constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  handleClose = event => {
    this.setState({ showModal: false });
  }

  handleShow = event => {
    this.setState({ showModal: !this.state.showModal });
  }

	render() {
		return (
			<div onClick={this.handleShow}>
				<Thumbnail src={this.props.itemVideo.cover} className="item-video">
		      <h4>{this.props.itemVideo.title}</h4>
		      <p>{this.props.itemVideo.author}</p>
		      <p>{this.props.itemVideo.views}</p>
		      <p>{this.props.itemVideo.date}</p>
		    </Thumbnail>
		    <ModalVideo showModal={this.state.showModal} handleClose={this.handleClose} video={this.props.itemVideo}/>

		  </div>
		)
	}
}


export default ItemVideo;