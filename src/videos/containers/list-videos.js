import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import ItemVideo from '../components/item-video';



class ListVideos extends Component {

	render() {
		return (
			<div>
				<h2 className="text-center">Lista de videos</h2>
				<Grid>
					<Row>
						{
							this.props.listVideos.map((video) => {
								return (
									<Col md={3} key={this.props.id + video.id}>
										<div>
											<ItemVideo itemVideo={video} />
										</div>	
									</Col>
								)
							})
						}
					</Row>
				</Grid>

			</div>
		)
	}
}


export default ListVideos;